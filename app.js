var express = require('express');

var app = express();

console.log('server starting');

app.get('/', function(req, res){
    res.send('Hello Internet.org from NodeJS inside of a docker container, I am listening on port 8080!');
});

app.listen(8080);